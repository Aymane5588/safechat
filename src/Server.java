import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Scanner;

public class Server {

  private int port;
  private List<User> clients;
  private ServerSocket server;

  public static void main(String[] args) throws IOException, ClassNotFoundException {
    new Server(12345).run();
  }

  public Server(int port) {
    this.port = port;
    this.clients = new ArrayList<User>();
  }

  public void run() throws IOException, ClassNotFoundException {
    server = new ServerSocket(port) {
      protected void finalize() throws IOException {
        this.close();
      }
    };
    System.out.println("Port 12345 is now open.");

    while (true) {
      // accepts a new client
      Socket client = server.accept();
      if (this.clients.size() < 2) {
        // get nickname of newUser
        String nickname = (new Scanner(client.getInputStream())).nextLine();
        nickname = nickname.replace(",", ""); //  ',' use for serialisation
        nickname = nickname.replace(" ", "_");
        System.out.println("New Client: \"" + nickname + "\"\n\t     Host:" + client.getInetAddress().getHostAddress());
        ObjectInputStream inputKey = new ObjectInputStream(client.getInputStream());
        RSAPublicKey key = (RSAPublicKey) inputKey.readObject();
        // create new User
        User newUser = new User(client, nickname, key);
        // add newUser message to list
        this.clients.add(newUser);
        // Welcome msg
        newUser.getOutStream().println("# Welcome " + newUser.toString() + " #");
        // create a new thread for newUser incoming messages handling
        new Thread(new UserHandler(this, newUser)).start();
      }
    }
  }

  // delete a user from the list
  public void removeUser(User user) {
    this.clients.remove(user);
  }

  // send incoming msg to all Users
  public void broadcastMessages(String msg, User userSender, boolean key) {
    for (User client : this.clients)
    {
      if (key == false)
        client.getOutStream().println(userSender.toString()+ "##" + msg);
      else
        client.getOutStream().println("--" + userSender.toString()+ "##" + msg);
    }
  }

  // send list of clients to all Users
  public void broadcastAllUsers() {
    for (User client : this.clients) {
      client.getOutStream().println(this.clients);
    }
  }

  public List<User> getClients() {
    return clients;
  }
}

class UserHandler implements Runnable {

  private Server server;
  private User user;

  public UserHandler(Server server, User user) {
    this.server = server;
    this.user = user;
    this.server.broadcastAllUsers();
  }

  public void run() {
    String message;

    // when there is a new message, broadcast to all
    Scanner sc = new Scanner(this.user.getInputStream());
    if (server.getClients().size() == 2)
    {
      try {
      KeyFactory kf = KeyFactory.getInstance("RSA");
      X509EncodedKeySpec keySpecX509 = kf.getKeySpec(user.getPublicKey(), X509EncodedKeySpec.class);
      byte[] encoded =  Base64.getDecoder().decode(keySpecX509.getEncoded());
      String keyToSend = ReceivedMessagesHandler.Hex(encoded);
      server.broadcastMessages(keyToSend, user, true);
      } catch (Exception e) {
        //TODO e.printStackTrace();
      }
    }
    while (sc.hasNextLine()) {
      message = sc.nextLine();
      server.broadcastMessages(message, user, false);
    }
    // end of Thread
    server.removeUser(user);
    this.server.broadcastAllUsers();
    sc.close();
  }
}

class User {
  private static int nbUser = 0;
  private int userId;
  private PrintStream streamOut;
  private InputStream streamIn;
  private String nickname;
  private Socket client;
  private RSAPublicKey publicKey;

  // constructor
  public User(Socket client, String name, RSAPublicKey key) throws IOException {
    this.streamOut = new PrintStream(client.getOutputStream());
    this.streamIn = client.getInputStream();
    this.client = client;
    this.nickname = name;
    this.userId = nbUser;
    this.publicKey = key;
    nbUser += 1;
  }

  // getteur
  public PrintStream getOutStream(){
    return this.streamOut;
  }

  public InputStream getInputStream(){
    return this.streamIn;
  }

  public String getNickname(){
    return this.nickname;
  }

  // print user with his color
  public String toString(){

    return this.getNickname();

  }

  public RSAPublicKey getPublicKey() {
    return publicKey;
  }
}