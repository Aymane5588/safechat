import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Base64;

import static java.lang.System.exit;


public class Client {

  private String host;
  private int port;
  private KeyPair keys;
  private PublicKey otherkey;

  public static void main(String[] args) throws IOException {
    if (args.length < 2) {
      System.err.println("Specify public key as parameter 1 and private key as parameter 2");
      exit(-1);
    }
    else
      new Client("127.0.0.1", 12345, args[0], args[1]).run();
  }

  public Client(String host, int port, String publicPath, String privatePath) {
    this.host = host;
    this.port = port;
    RSAPublicKey publicKey = getpublicKey(Paths.get(publicPath));
    RSAPrivateKey privateKey = getprivateKey(Paths.get(privatePath));
    this.keys = new KeyPair(publicKey, privateKey);
    this.otherkey = publicKey;
  }

  public void run() throws IOException {
    // connect client to server
    Socket client = new Socket(host, port);
    System.out.println("Client successfully connected to server!");

    // Get Socket output stream (where the client send her mesg)
    PrintStream output = new PrintStream(client.getOutputStream());

    // ask for a nickname
    Scanner sc = new Scanner(System.in);
    System.out.print("Enter a nickname: ");
    String nickname = sc.nextLine();

    // send nickname to server
    output.println(nickname);
    ObjectOutputStream outputStream = new ObjectOutputStream(client.getOutputStream());
    outputStream.writeObject(this.keys.getPublic());

    // create a new thread for server messages handling
    new Thread(new ReceivedMessagesHandler(client.getInputStream(), this, nickname)).start();

    // read messages from keyboard and send to server
    System.out.println("Messages: \n");

    // while new messages
    while (sc.hasNextLine()) {
      String messageclear = sc.nextLine();
      byte[] messageClearByte = messageclear.getBytes();
      try {
        byte[] encryptedMessage = encrypt((RSAPublicKey) this.otherkey, messageClearByte);
        String messageToSend = ReceivedMessagesHandler.Hex(encryptedMessage);
        System.out.println(nickname + " : " + messageclear);
        output.println(messageToSend);
      } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
        e.printStackTrace();
      }
      //output.println(sc.nextLine());
    }

    // end ctrl D
    output.close();
    sc.close();
    client.close();
  }

  public void setOtherkey(PublicKey otherkey) {
    this.otherkey = otherkey;
  }

  public KeyPair getKeys() {
    return keys;
  }

  public static RSAPublicKey getpublicKey(Path path) {

    try {
      //Paths.get(ClassLoader.getSystemResource("public_key.pem").toURI())
      String publicKeyContent = new String(Files.readAllBytes(path));
      publicKeyContent = publicKeyContent.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
      X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
      KeyFactory kf = KeyFactory.getInstance("RSA");
      return (RSAPublicKey) kf.generatePublic(keySpecX509);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static RSAPrivateKey getprivateKey(Path path) {

    try {
      //Paths.get(ClassLoader.getSystemResource("private_key_pkcs8.pem").toURI()))
      String privateKeyContent = new String(Files.readAllBytes(path));
      privateKeyContent = privateKeyContent.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
      PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
      KeyFactory kf = KeyFactory.getInstance("RSA");
      return (RSAPrivateKey)kf.generatePrivate(keySpecPKCS8);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public static byte[] encrypt(RSAPublicKey key, byte[] plaintext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
  {
    Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.ENCRYPT_MODE, key);
    return cipher.doFinal(plaintext);
  }

  public static byte[] decrypt(RSAPrivateKey key, byte[] ciphertext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException
  {
    Cipher cipher = Cipher.getInstance("RSA");
    cipher.init(Cipher.DECRYPT_MODE, key);
    return cipher.doFinal(ciphertext);
  }

}

class ReceivedMessagesHandler implements Runnable {

  private InputStream server;
  private Client client;
  private String nickname;

  public ReceivedMessagesHandler(InputStream server, Client client, String nickname) {
    this.server = server;
    this.client = client;
    this.nickname = nickname;
  }

  public void run() {
    // receive server messages and print out to screen
    Scanner s = new Scanner(server);
    String tmp = "";
    while (s.hasNextLine()) {
      tmp = s.nextLine();
      if (tmp.charAt(0) == '[') {
        tmp = tmp.substring(1, tmp.length()-1);
        System.out.println(
                "\nUsers connected: " +
                        new ArrayList<String>(Arrays.asList(tmp.split(","))) + "\n"
        );
      }
      else if (tmp.charAt(0) == '#')
        System.out.println(tmp);
      else if (tmp.charAt(0) == '#')
        System.out.println(tmp);
      else {
        if (tmp.charAt(0) == '-' && tmp.charAt(1) == '-') {
          String[] response = tmp.split("##");
          if (response[0].equals("--" + nickname) == false)
          {
            byte[] encoded = UnHex(response[1]);
            try {
              //Paths.get(ClassLoader.getSystemResource("public_key.pem").toURI())
              String publicKeyContent = new String(encoded.toString());
              X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
              KeyFactory kf = KeyFactory.getInstance("RSA");
              this.client.setOtherkey(kf.generatePublic(keySpecX509));
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        } else {
          try {
            String[] response = tmp.split("##");
            if (response[0].equals(nickname) == false) {
              System.out.print(response[0] + " : ");
              byte[] encoded = UnHex(response[1]);
              byte[] message = Client.decrypt((RSAPrivateKey) this.client.getKeys().getPrivate(), encoded);
              String mess = new String(message);
              System.out.println(mess);
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
    }
    s.close();
  }


  public static byte[] UnHex(String ss) {
    int strlen = ss.length();
    if (strlen>0 && strlen%2!=0) {
      throw new RuntimeException("odd-length string");
    }
    byte[] ret = new byte[strlen/2];
    for (int i=0; i<strlen; i+=2) {
      int a = Character.digit(ss.charAt(i),0x10);
      int b = Character.digit(ss.charAt(i+1),0x10);
      if (a==-1 || b==-1) {
        throw new RuntimeException("non-hex digit");
      }
      ret[i/2] = (byte) ((a<<4)+b);
    }
    return ret;
  }
  private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
  public static String Hex(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    for ( int j = 0; j < bytes.length; j++ ) {
      int v = bytes[j] & 0xFF;
      hexChars[j * 2] = hexArray[v >>> 4];
      hexChars[j * 2 + 1] = hexArray[v & 0x0F];
    }
    return new String(hexChars);
  }
}
